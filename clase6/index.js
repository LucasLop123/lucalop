function Perro (nombre, edad, raza, tamaño, estado) {
        this.nombre = nombre;
        this.edad = edad;
        this.raza = raza;
        this.tamaño = tamaño;
        this.estado = estado;
    }

Perro.prototype.modificarEstado= function(){
    this.estado="en adopción"; 
    }

Perro.prototype.informarEstado= function(){
    console.log( this.nombre + ' tiene ' + this.edad + ' años y es raza ' + this.raza +'. Su estado actual es: ' + this.estado);
}

// ejemplo con un perro 
let corbata= new Perro("corbata",4,"galgo","mediano","adoptado")

corbata.informarEstado();
corbata.modificarEstado();
corbata.informarEstado();

 let perros = [];
// ingreso de datos. 
 function ingreso() {
     let nombre = prompt('Introduce el nombre del perro');
     let edad = prompt('Introduce la edad del perro (años)');
     let raza = prompt('Introduce la raza del perro');
     let tamaño = prompt('Introduce el tamaño del perro');
     let estado = prompt('Introduce el estado del perro (opciones: en adopción, en proceso de adopción, adoptado )');
    // si algún campo no contiene datos o el estado no es uno de los 3 permitidos no ingresa el objeto dentro del array. 
     if (nombre !== "" && edad != "" && raza != "" && tamaño != "" && (estado == 'en adopción' || estado == "en proceso de adopción" || estado == "adoptado")) {
         perros.push(new Perro(nombre, edad, raza, tamaño, estado));
     }
     // Si presiona boton aceptar ingresa otro perro, sino muestra los informes. 
    var otroIngreso= confirm("desea ingresar otro perro?");
     if(otroIngreso==true){
         ingreso()
     } else{
         verTodo();
     }
 }

 function verTodo() {

    // muestra el listado de perros completo. 
     console.log("Referencias( N:Nombre , E: Edad, R: Raza , T: tamaño, ES:Estado )");
     console.log("El listado de todos los perros es el siguiente: ");

     for (const perro of perros) {
         console.log(`N: ${perro.nombre}, E: ${perro.edad}, R: ${perro.raza}, T: ${perro.tamaño}, ES: ${perro.estado}`)
     }

    // muestra el listado de perros en adopción. 
     console.log("Los perros en adopción son los siguientes: ");
     let a=0;
        for (const perro of perros) {
            if (perro.estado == "en adopción") {
                console.log(`N: ${perro.nombre}, E: ${perro.edad}, R: ${perro.raza}, T: ${perro.tamaño}`);
                a= 1;
            }
        }   
        if(a==0){
            console.log("No hay perros en adopción");
        }

        // muestra el listado de perros en proceso de adopción. 
     console.log("Los perros en proceso de adopción son los siguientes: ");
        let b=0;
        for (const perro of perros) {
            if (perro.estado == "en proceso de adopción") {
                console.log(`N: ${perro.nombre}, E: ${perro.edad}, R: ${perro.raza}, T: ${perro.tamaño}`)
                b=1;
            }
        }
        if(b==0){
            console.log("No hay perros en proceso de adopción");
        }

    // muestra el listado de perros adoptados. 
     console.log("Los perros adoptados son los siguientes: ");
     let c=0;

        for (const perro of perros) {
            if (perro.estado == "adoptado") {
                console.log(`N: ${perro.nombre}, E: ${perro.edad}, R: ${perro.raza}, T: ${perro.tamaño}`)
                c=1;
            }
        }
        if(c==0){
            console.log("No hay perros adoptados");
        }
 }


 ingreso();


//  modificarEstadoenArray(){ 
//      let nom = prompt("Usted está por cambiar un estado. Ingrese el nombre del perro");
//      for (const perro of perros) {
//          if (perro.nombre == nom) {
//             let cambio = prompt("ingrese el nuevo estado del perrito");
//                 if (cambio != perro.estado & (cambio == 'en adopción' || cambio == "en proceso de adopción" || cambio == "adoptado")) {
//                 perro.estado = cambio;
//                 console.log("El nuevo estado del perro es el siguiente: " + cambio);
//                 } else {
//                 console.log("El cambio que desea hacer no es posible")
//             }
//         } else {
//             console.log("No existe un perro con ese nombre en la base de datos")
//         }
//     }
// }